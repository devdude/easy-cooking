<?php

namespace EasyCooking\FixturesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EasyCooking\SalesFloorBundle\Entity\Condiment;
use EasyCooking\SalesFloorBundle\Entity\Meal;

/**
 * Class MealData
 * @package EasyCooking\FixturesBundle\DataFixtures\ORM
 * @author Marcel Meyer <marcel@devdu.de>
 */
class MealData extends AbstractFixture implements OrderedFixtureInterface
{

    const IMAGE_PATH = '/files/images/meals/';

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     * @param \Doctrine\Common\Persistence\ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /*
            [
                'name' => '',
                'subtitle' => '',
                'description' => '',
                'price' => ,
                'difficultLevel' => ,
                'preparationTime' => ,
                'image' => '',
                'categories' => [],
                'condiments' => [],
            ],
         */

        $meals = [
            [
                'name' => 'Schweinefilet mit Crème-fraîche-Wirsing-Soße',
                'subtitle' => 'verfeinert mit Schinkenspeck und Spätzle',
                'description' => 'Hast Du Dich je gefragt, wie es die Leute früher ausgehalten haben im Winter ständig Kohl zu essen? Nun, mit unserem Wirsingrezept kommt es einem nicht mehr so hart vor, das darf ruhig öfters auf den Tisch kommen. Spätzle sorgen für Energie und Schweinefilet stärkt die Muskeln. Guten Appetit.',
                'price' => 7.4,
                'difficultLevel' => 2,
                'preparationTime' => 40,
                'image' => 'schweinefilet-wirsing.jpg',
                'categories' => [
                    CategoryData::MEAT,
                ],
                'condiments' => [
                    CondimentData::SCHWEINEFILET => 120,
                    CondimentData::SCHINKENSPECK => 25,
                    CondimentData::WIRSING => 0.25,
                    CondimentData::SCHALOTTE => 1,
                    CondimentData::KNOBLAUCH => 1,
                    CondimentData::ROSMARIN => 1,
                    CondimentData::CREME_FRAICHE => 40,
                    CondimentData::SPAETZLE => 200,
                    CondimentData::SCHNITTLAUCH => 5,
                ],
            ],
            [
                'name' => 'Rinderhüftsteak à la Strindberg',
                'subtitle' => 'mit Bratkartoffeln und Karottensalat',
                'description' => 'Ein Drittel Teller voll Eiweiß, ein Drittel voll Kohlehydrate und ein Drittel voll sekundärer Pflanzenstoffe = ein perfektes, ausgewogenes Essen. Zudem geht es heute auch wieder sehr bunt auf dem Teller zu und harmoniert wunderschön, damit auch Deine Augen satt werden. Lass es Dir schmecken!',
                'price' => 9.6,
                'difficultLevel' => 2,
                'preparationTime' => 45,
                'image' => 'rinderhueftsteak-strindberg.jpg',
                'categories' => [
                    CategoryData::MEAT,
                ],
                'condiments' => [
                    CondimentData::RINDERHUEFTSTEAKS => 125,
                    CondimentData::KARTOFFELN => 300,
                    CondimentData::ZWIEBEL => 0.5,
                    CondimentData::CLEMENTINE => 0.5,
                    CondimentData::KAROTTEN => 1,
                    CondimentData::OREGANO => 1,
                ],
            ],

            [
                'name' => 'Fusilli Bucatini Kürbis Pasta',
                'subtitle' => 'mit Salbei, Mozzarella und Pecorino',
                'description' => 'Günstig, schnell und gesund. Italienisches Flair mit würzigem Kürbis!',
                'image' => 'fusilli-bucatini-kuerbis-pasta.jpg',
                'difficultLevel' => 1,
                'preparationTime' => 30,
                'price' => 3.20,
                'categories' => [
                    CategoryData::VEGGIE,
                ],
                'condiments' => [
                    CondimentData::FUSILLI_BUCATI => 125,
                    CondimentData::HOKKAIDOKUERBIS => 0.25,
                    CondimentData::PECORINO => 10,
                    CondimentData::MOZZARELLA => 70,
                    CondimentData::SALBEI => 2.5,
                    CondimentData::KNOBLAUCH => 1,
                ],
            ],
            [
                'name' => 'Marinierte Schweinemedaillons mit Hasselback-Kartoffeln',
                'subtitle' => 'selbstgemachtem Kräuterquark und Salatmix “oriental”',
                'description' => 'Die Zubereitung als Fächerkartoffel wurde in Schweden im Hotel Hasselbacken kreiert, woher sich der Name Hasselback herleitet. Du kannst sie auch als Hauptgericht verwenden und die Spalten abwechselnd mit Bacon und Käse füllen. Heute lässt Du Dir dazu aber Schweinemedaillons und einen frischen Salat schmecken.',
                'difficultLevel' => 3,
                'preparationTime' => 40,
                'price' => 4.25,
                'categories' => [
                    CategoryData::MEAT,
                ],
                'image' => 'marinierte_schweinemedaillons_mit_hasselback_kartoffeln.jpg',
                'condiments' => [
                    CondimentData::KAROTTEN => 300,
                    CondimentData::PETERSILIE => 5,
                    CondimentData::SCHNITTLAUCH => 5,
                    CondimentData::QUARK => 125,
                    CondimentData::SCHWEINEFILET => 200,
                    CondimentData::SALATMIX => 25,
                ],
            ],
            [

                'name' => 'Marokkanische Zucchini-Hähnchenbrust',
                'subtitle' => 'mit Erbsen-Basmatireis und Petersilien-Senf-Joghurt',
                'description' => 'Kennst Du rosa Pfeffer? Eigentlich sind es ja rosa Beeren, die näher mit Mangos und Pistazien verwandt sind als mit echtem Pfeffer. Geschmacklich haben die rosa Beeren es in sich: Sie schmecken nur ganz leicht scharf. Tipp: Die Beeren leicht andrücken, wenn Du sie zur Hähnchenbrust gibst, und genießen!',
                'difficultLevel' => 2,
                'preparationTime' => 40,
                'price' => 5,
                'image' => 'marokkanische_zucchini_haehnchenbrust.jpg',
                'categories' => [
                    CategoryData::POULTRY,
                ],
                'condiments' => [
                    CondimentData::HAEHNCHENBRUST => 125,
                    CondimentData::BASMATIREIS => 75,
                    CondimentData::ERBSEN => 50,
                    CondimentData::ZUCCHINI => 1,
                    CondimentData::ZWIEBEL => 1,
                    CondimentData::PETERSILIE => 5,
                    CondimentData::JOGHURT => 100,

                ],
            ],
            [
                'name' => 'Vegan-Döner',
                'subtitle' => 'mit Selbstgemachtem Tsatsiki in Pita Taschen',
                'description' => 'Ganz ohne Fleisch aber mindestens so lecker, wie ein „klassischer“ Döner.',
                'difficultLevel' => 1,
                'preparationTime' => 35,
                'price' => 3.7,
                'image' => 'vegan_doener.jpg',
                'categories' => [
                    CategoryData::VEGAN,
                    CategoryData::VEGGIE,
                ],
                'condiments' => [
                    CondimentData::WHEATY_VEGAN_DOENER => 100,
                    CondimentData::PITA_TASCHEN => 1,
                    CondimentData::TOMATE => 1,
                    CondimentData::SALATHERZEN => 1,
                    CondimentData::KAROTTEN => 2,
                    CondimentData::JOGHURT => 80,
                    CondimentData::KNOBLAUCH => 1,
                    CondimentData::ZITRONE => 0.25,
                    CondimentData::GURKE => 0.25,
                ],
            ],
            [
                'name' => 'Vegane Chorizo-Süßkartoffelpfanne',
                'subtitle' => 'verfeinert mit frischen Tomaten und Schwarzem-Bohnen-Püree',
                'description' => 'Ausgefallenes Menü – auch für Anfänger geeignet und ganz schnell zubereitet.',
                'difficultLevel' => 1,
                'preparationTime' => 35,
                'price' => 2.9,
                'image' => 'vegane_chorizo_suesskartoffelpfanne.jpg',
                'categories' => [
                    CategoryData::VEGGIE,
                    CategoryData::VEGAN,
                ],
                'condiments' => [
                    CondimentData::VEGAN_CHORIZO => 0.5,
                    CondimentData::SUESSKARTOFFEL => 150,
                    CondimentData::SCHWARZE_BOHNEN => 0.5,
                    CondimentData::ZWIEBEL => 0.5,
                    CondimentData::TOMATE => 1,
                    CondimentData::PETERSILIE => 5,
                ],
            ],

            [
                'name' => 'Vollkornspaghetti mit veganer Bolognese',
                'subtitle' => 'verfeinert mit frischen Kräutern und Rucola',
                'description' => 'Tolle Alternative zur gewöhnlichen Bolognese – wird sicher auch Nicht-Veganern schmecken!',
                'difficultLevel' => 3,
                'preparationTime' => 25,
                'price' => 3.3,
                'categories' => [
                    CategoryData::VEGGIE,
                    CategoryData::VEGAN,
                ],
                'image' => 'vollkornspaghetti_mit_veganer_bolognese.jpg',
                'condiments' => [
                    CondimentData::VOLLKORNSPAGHETTI => 100,
                    CondimentData::ZWIEBEL => 0.5,
                    CondimentData::KNOBLAUCH => 1,
                    CondimentData::TOFU => 50,
                    CondimentData::KAROTTEN => 1,
                    CondimentData::PIZZA_TOMATEN => 0.5,
                    CondimentData::COUSCOUS => 50,
                    CondimentData::RUCOLA => 20,
                ],
            ],
            [
                'name' => 'Mediterranes Grill-Gemüse',
                'subtitle' => 'mit Tomaten-Kichererbsen-Soße und Walnuss-Koriander-Joghurt',
                'description' => 'Dieses Rezept eignet sich auch prima für Grillpartys. Die Soße und den Joghurt kannst Du vorbereiten und das Gemüse vor Ort frisch grillen. Ohne den Joghurt machst Du damit sogar Deine vegane Gäste glücklich. Genieß Dein Essen und schönes Grillwetter dazu! Guten Appetit.',
                'image' => 'mediterranes_grill_gemuese.jpg',
                'difficultLevel' => 3,
                'preparationTime' => 40,
                'price' => 3.1,
                'categories' => [
                    CategoryData::VEGGIE,
                    CategoryData::VEGAN,
                ],
                'condiments' => [
                    CondimentData::ZWIEBEL => 1,
                    CondimentData::INGWER => 5,
                    CondimentData::KICHERERBSEN => 0.5,
                    CondimentData::AUBERGINE => 0.5,
                    CondimentData::ZUCCHINI => 0.5,
                    CondimentData::TOMATE => 1,
                    CondimentData::LIMETTE => 0.5,
                    CondimentData::WALNUESSE => 10,
                    CondimentData::KNOBLAUCH => 1,
                    CondimentData::KORIANDER => 5,
                    CondimentData::JOGHURT => 100,
                    CondimentData::KAROTTEN => 100,
                ],
            ],

            [
                'name' => 'Pikante Thunfisch-Burger',
                'subtitle' => 'mit Zitronen-Mayonnaise und knusprigen Kartoffel-Wedges',
                'description' => 'Burger mal anders – lass dich vom pikanten Thunfisch und einer würzigen Soße überzeugen.',
                'difficultLevel' => 2,
                'preparationTime' => 25,
                'price' => 4.1,
                'image' => 'pikante_thunfisch_burger.jpg',
                'categories' => [
                    CategoryData::FISH,
                ],
                'condiments' => [
                    CondimentData::THUNFISCH => 0.5,
                    CondimentData::KAROTTEN => 1,
                    CondimentData::SEMMELBROESEL => 25,
                    CondimentData::SCHALOTTE => 1,
                    CondimentData::PETERSILIE => 5,
                    CondimentData::CAYENNEPFEFFER => 2,
                    CondimentData::SALATHERZEN => 0.5,
                    CondimentData::SESAM_BUNS => 1,
                    CondimentData::KARTOFFELN => 150,
                    CondimentData::MAYONNAISE => 20,
                    CondimentData::ZITRONE => 0.5,
                    CondimentData::TOMATE => 1,
                ],
            ],

            [
                'name' => 'Delikate Stremellachs-Pizza',
                'subtitle' => 'mit Dill-Frischläse, garniert mit Rucola',
                'description' => 'Frischer Fisch ist besonders empfindlich und kann schon bei einer Temperaturabweichung von 0,5 Grad verderben. Aber zum Glück gibt es den leckeren Stremellachs, der heiß geräuchert wird und dadurch schön saftig und lange frisch bleibt. Und sehr gesund ist er wegen seiner Omega-3-Fettsäuren natürlich auch.',
                'difficultLevel' => 1,
                'preparationTime' => 20,
                'price' => 2.8,
                'image' => 'delikate_stremellachs_pizza.jpg',
                'categories' => [
                    CategoryData::FISH,
                ],
                'condiments' => [
                    CondimentData::DILL => 5,
                    CondimentData::ZWIEBEL => 0.5,
                    CondimentData::RUCOLA => 50,
                    CondimentData::ZITRONE => 0.5,
                    CondimentData::STREMELLACHS => 80,
                    CondimentData::FRISCHKAESE => 50,
                    CondimentData::PIZZATEIG => 1,
                ],
            ],

            [
                'name' => 'Thunfisch-Penne mit Pesto Rosso',
                'subtitle' => 'und gelben Kirschtomaten',
                'description' => 'Pesto selber zu machen ist gar nicht schwer, und es lässt sich vor allem unendlich variieren und an persönliche Vorlieben anpassen. Wenn du dafür sorgst, dass das Pesto an der Oberfläche mit Öl bedeckt ist, kannst du es zudem einige Zeit im Kühlschrank aufbewahren. Lass es Dir schmecken!',
                'difficultLevel' => 1,
                'preparationTime' => 20,
                'price' => 3.3,
                'image' => 'thunfisch_penne_mit_pesto_rosso.jpg',
                'categories' => [
                    CategoryData::FISH,
                ],
                'condiments' => [
                    CondimentData::PENNE => 125,
                    CondimentData::PARMESAN => 10,
                    CondimentData::KIRSCHTOMATEN => 50,
                    CondimentData::THUNFISCH => 1,
                    CondimentData::PETERSILIE => 5,
                    CondimentData::KNOBLAUCH => 1,
                    CondimentData::MANDELN => 5,
                    CondimentData::TOMATEN_GETROCKNET => 25,
                ],
            ],

            [
                'name' => 'Cremige Meerrettich-Frischkäse-Spaghetti',
                'subtitle' => 'mit Räucherlachs, gerösteten Kürbiskernen und Rucola-Salat',
                'description' => 'Falls Du Meerrettich bisher nur fertig aus dem Glas kanntest, kannst Du den Rettich heute einmal selbst verarbeiten - und dich auf gesunde Glucosinolate freuen, die Senföle, die dem Rettich seine Schärfe verleihen. Und weil Meerrettich super zu Fisch passt, gibt es heute Lachs dazu - und Nudeln, versteht sich, denn die passen immer, wie Du ja längst weißt. Guten Appetit!',
                'difficultLevel' => 1,
                'preparationTime' => 25,
                'price' => 3.8,
                'image' => 'cremige_meerrettich_frischkaese_spaghetti.jpg',
                'categories' => [
                    CategoryData::FISH,
                ],
                'condiments' => [
                    CondimentData::SPAGHETTI => 250,
                    CondimentData::KUERBISKERNE => 10,
                    CondimentData::ZITRONE => 0.5,
                    CondimentData::RUCOLA => 30,
                    CondimentData::MEERRETTICH => 15,
                    CondimentData::DILL => 5,
                    CondimentData::FRISCHKAESE => 75,
                    CondimentData::RAEUCHERLACHS => 0.5,
                ],
            ],

            [
                'name' => 'Kartoffelpfanne Italia mit Mozzarella',
                'subtitle' => 'selbstgemachtem Pesto Rosso und verfeinert mit Rucola',
                'description' => 'Eine schnelle Vorspeise? 1 Zucchini längs in dünne Scheiben hobeln. 100 g Feta, 50 g Frischkäse, 1 TL Semmelbrösel, 1 TL Parmesan, Salz und Pfeffer verrühren, Zucchinischeiben damit bestreichen, aufrollen, jeweils mit einem Zahnstocher fixieren und für ca. 5-10 Min. bei 180 Grad in den Ofen schieben.',
                'difficultLevel' => 2,
                'preparationTime' => 35,
                'price' => 4.8,
                'image' => 'kartoffelpfanne_italia_mit_mozzarella.jpg',
                'categories' => [
                    CategoryData::VEGGIE,
                ],
                'condiments' => [
                    CondimentData::KARTOFFELN => 300,
                    CondimentData::PAPRIKA_GELB => 0.5,
                    CondimentData::ZWIEBEL => 0.5,
                    CondimentData::RUCOLA => 50,
                    CondimentData::BASILIKUM => 5,
                    CondimentData::MOZZARELLA => 80,
                    CondimentData::HARTKAESE_GERIEBEN => 10,
                    CondimentData::MANDELN => 10,
                    CondimentData::KNOBLAUCH => 1,
                    CondimentData::TOMATEN_GETROCKNET => 25,
                    CondimentData::CHILIFLOCKEN => 1,
                ],
            ],

            [
                'name' => 'Köstliche Speck-Spätzle in Orangen-Sahnesoße',
                'subtitle' => 'mit Salat in fruchtiger Orangen-Vinaigrette',
                'description' => 'Kreative Küche heißt, altbekannte Gerichte immer wieder neu zu erfinden. Durch die Orange bekommen die Speck-Spätzle eine sommerlich fruchtige Note, welche ein ganz neues Gericht daraus macht. Und die Orange lässt sich im Salat gleich noch einmal mit demselben Effekt verarbeiten. Guten Appetit!',
                'difficultLevel' => 1,
                'preparationTime' => 25,
                'price' => 4.2,
                'image' => 'koestliche_speck_spaetzle_in_orangen_sahnesosse.jpg',
                'categories' => [
                    CategoryData::MEAT,
                ],
                'condiments' => [
                    CondimentData::SAFTORANGE => 1,
                    CondimentData::PETERSILIE => 5,
                    CondimentData::ZWIEBEL => 0.5,
                    CondimentData::SCHINKENSPECK => 50,
                    CondimentData::SPAETZLE => 200,
                    CondimentData::SALATMIX => 50,
                    CondimentData::MUSKATNUSS => 1,
                    CondimentData::SAHNE => 50,
                ],
            ],

            [
                'name' => 'Indisches Korma-Gemüsecurry',
                'subtitle' => 'mit Naan-Brot und frischem Koriander',
                'description' => 'Das bunte indische Leben können wir Dir nicht ins Haus holen, aber ein bisschen indische Küche. Mithilfe der richtigen Gewürzmischung und der fertigen Naan-Brote kann geschmacklich nichts schiefgehen und Du kannst Dich gefahrlos dem Fernweh hingeben und genießen. Namaste!',
                'difficultLevel' => 2,
                'preparationTime' => 40,
                'price' => 2.95,
                'image' => 'indisches_korma_gemuesecurry.jpg',
                'categories' => [
                    CategoryData::VEGGIE,
                ],
                'condiments' => [
                    CondimentData::NAAN_BROTE => 1,
                    CondimentData::BUTTERNUSS_KUERBIS => 0.5,
                    CondimentData::ZUCCHINI => 0.5,
                    CondimentData::PAPRIKA_ROT => 0.5,
                    CondimentData::ZWIEBEL => 0.5,
                    CondimentData::TOMATENMARK => 10,
                    CondimentData::KOKOSMILCH => 125,
                    CondimentData::KORIANDER => 5,
                    CondimentData::ZITRONE => 0.5,
                ],
            ],

            [
                'name' => 'Bunter Couscous-Salat mit gebackener Aubergine, Paprika',
                'subtitle' => 'und Basilikum-Minz-Mozzarella',

                'description' => 'Ein Couscous-Salat passt nicht nur prima zu einem Grillabend, sondern besteht mit den richtigen Zutaten auch als Hauptgericht. In diesem Fall sind Auberginen und Mandeln die richtigen Zutaten, die zudem für sekundäre Pflanzenstoffe und essenzielle Fettsäuren sorgen und satt machen. Guten Appetit!',
                'difficultLevel' => 2,
                'preparationTime' => 30,
                'price' => 4.1,
                'image' => 'bunter_couscous_salat_mit_gebackener_aubergine_paprika.jpg',
                'categories' => [
                    CategoryData::VEGGIE,
                ],
                'condiments' => [
                    CondimentData::AUBERGINE => 0.5,
                    CondimentData::PAPRIKA_ROT => 1,
                    CondimentData::COUSCOUS => 80,
                    CondimentData::MOZZARELLA => 80,
                    CondimentData::ZITRONE => 0.5,
                    CondimentData::CHILISCHOTE_ROT => 1,
                    CondimentData::BASILIKUM => 5,
                    CondimentData::RUCOLA => 30,
                    CondimentData::MANDELN => 10,
                    CondimentData::KIRSCHTOMATEN => 100,
                ],
            ],

            [
                'name' => 'Feine Prosciutto-Kirschtomaten-Tarte',
                'subtitle' => 'mit Ricotta und frischem Basilikum',

                'description' => 'Ein herzhafter Kuchen, der Groß und Klein schmeckt! Wie Pizza – nur besser.',
                'difficultLevel' => 1,
                'preparationTime' => 30,
                'price' => 2.85,
                'image' => 'feine_prosciutto_kirschtomaten_tarte.jpg',
                'categories' => [
                    CategoryData::VEGGIE,
                ],
                'condiments' => [
                    CondimentData::BLAETTERTEIG => 0.5,
                    CondimentData::BASILIKUM => 5,
                    CondimentData::PROSCIUTTO => 2,
                    CondimentData::RICOTTA => 75,
                    CondimentData::KNOBLAUCH => 1,
                    CondimentData::KIRSCHTOMATEN => 50,
                ],
            ],

            [
                'name' => 'Veganer Kartoffel-Zucchini-Auflauf',
                'subtitle' => 'mit Räuchertofu und Tomaten-Soja-Pesto',

                'description' => 'Räuchertofu verleiht diesem Gericht den besonderen pikanten Geschmack. Das Pesto kannst Du auch als Brotaufstrich verwenden, wenn Du die Sojasahne weglässt, damit es ein eher fester, cremiger Aufstrich wird. Wenn Du es lieber salzarm magst, kannst Du die getrockneten Tomaten vorher mit warmem Wasser abwaschen. Guten Appetit!',
                'difficultLevel' => 2,
                'preparationTime' => 25,
                'price' => 5.5,
                'image' => 'veganer_kartoffel_zucchini_auflauf.jpg',
                'categories' => [
                    CategoryData::VEGGIE,
                    CategoryData::VEGAN,
                ],
                'condiments' => [
                    CondimentData::KARTOFFELN => 300,
                    CondimentData::ZUCCHINI => 0.5,
                    CondimentData::RAEUCHERTOFU => 50,
                    CondimentData::SCHNITTLAUCH => 5,
                    CondimentData::TOMATEN_GETROCKNET => 50,
                    CondimentData::HEFEFLOCKEN => 2,
                    CondimentData::SPEISESTAERKE => 5,
                    CondimentData::KURKUMA => 1,
                    CondimentData::SOJASAHNE => 50,
                ],
            ],

            [
                'name' => 'Cremig-feurige Thunfisch-Fettuccine',
                'subtitle' => 'mit frischem Oregano und Chiliflocken',
                'description' => 'Feurig, würzig und cremig: Mit Thunfisch und Chili wird dieses Gericht zum Geheimtipp für alle, die es scharf mögen!',
                'difficultLevel' => 1,
                'preparationTime' => 25,
                'price' => 3.35,
                'image' => 'cremig_feurige_thunfisch_fettuccine.jpg',
                'categories' => [
                    CategoryData::FISH,
                ],
                'condiments' => [
                    CondimentData::FETTUCCINE => 125,
                    CondimentData::CREME_FRAICHE => 25,
                    CondimentData::THUNFISCH => 0.5,
                    CondimentData::TOMATENMARK => 10,
                    CondimentData::OREGANO => 5,
                    CondimentData::ZWIEBEL => 0.5,
                    CondimentData::CHILIFLOCKEN => 1,
                    CondimentData::KNOBLAUCH => 1,
                ],
            ],

            [
                'name' => 'Schweinefleisch griechischer Art in Rosmarin-Thymian-Marinade',
                'subtitle' => 'Rosmarin-Thymian-Marinade mit gebratenem Feta, cremigem Kartoffelpüree und Tomaten-Zwiebelsalat',
                'description' => 'Deftiges Mittagessen mit griechischem Flair – lass dich von den mediterranen Gewürzen inspirieren.',
                'difficultLevel' => 3,
                'preparationTime' => 35,
                'price' => 6.2,
                'image' => 'schweinefleisch_griechischer_art_in_rosmarin_thymian_marinade.jpg',
                'categories' => [
                    CategoryData::MEAT,
                ],
                'condiments' => [
                    CondimentData::KARTOFFELN => 300,
                    CondimentData::SCHWEINEFILET => 200,
                    CondimentData::ZITRONE => 0.5,
                    CondimentData::FETA => 50,
                    CondimentData::KNOBLAUCH => 1,
                    CondimentData::THYMIAN => 5,
                    CondimentData::ROSMARIN => 5,
                    CondimentData::TOMATE => 1,
                    CondimentData::ZWIEBEL => 0.5,
                ],
            ],

            [
                'name' => 'Steak mit cremigem Chicorée',
                'subtitle' => 'und Frühlingszwiebel-Kartoffelstampf',
                'description' => 'Wie viel Fleisch soll ich essen? Die Deutsche Gesellschaft für Ernährung empfiehlt, nicht mehr als 300 bis 600 Gramm Fleisch pro Person und Woche zu essen. An diese Empfehlung halten wir uns, wenn wir die Gerichte für euch zusammenstellen. Eine ausgewogene Ernährung liegt uns am Herzen!',
                'difficultLevel' => 3,
                'preparationTime' => 30,
                'price' => 4,
                'image' => 'steak_mit_cremigem_chicoree.jpg',
                'categories' => [
                    CategoryData::MEAT,
                ],
                'condiments' => [
                    CondimentData::RINDERHUEFTSTEAKS => 125,
                    CondimentData::KARTOFFELN => 300,
                    CondimentData::KNOBLAUCH => 1,
                    CondimentData::THYMIAN => 5,
                    CondimentData::CHICOREE => 1,
                    CondimentData::BACON => 50,
                    CondimentData::SAHNE => 50,
                    CondimentData::SCHALOTTE => 1,
                ],
            ],

            [
                'name' => 'Hähnchenbrust mit knuspriger Cornflakes-Panade',
                'subtitle' => 'und mit Gouda überbackenem Kartoffel-Tomaten-Auflauf',
                'description' => 'Knusper, Knusper, Knäuschen ... Hähnchenschnitzel in Cornflakes-Panade und dazu auch noch Fett gespart, weil es im Ofen zubereitet wird und nicht in der Pfanne. Bald gibt es bei uns Geflügel mit einer Panade aus Nachos, Salzbrezeln oder viel- leicht auch wieder klassisch. Guten Appetit.',
                'difficultLevel' => 2,
                'preparationTime' => 30,
                'price' => 3.95,
                'image' => 'haehnchenbrust_mit_knuspriger_cornflakes_panade.jpg',
                'categories' => [
                    CategoryData::POULTRY,
                ],
                'condiments' => [
                    CondimentData::HAEHNCHENBRUST => 125,
                    CondimentData::CORNFLAKES => 30,
                    CondimentData::KARTOFFELN => 250,
                    CondimentData::RUCOLA => 30,
                    CondimentData::ZWIEBEL => 0.5,
                    CondimentData::TOMATE => 1,
                    CondimentData::GOUDA_GERIEBEN => 50,
                ],
            ],

            [
                'name' => 'Pikanter Geflügelwurst-Eintopf',
                'subtitle' => 'und Frühlingszwiebel-Kartoffelstampf',

                'description' => 'Die große Kunst des Kochens ist es, altbekannte Zutaten neu zu kombinieren. Darum gibt es heute keine Bratwurst mit Senf im Brötchen, sondern Bratwurst im Gemüseeintopf mit Kartoffelstampf. Passt perfekt zur Kälte draußen und versorgt dich mit Abwehrstoffen gegen Grippewellen. Guten Appetit!',
                'difficultLevel' => 2,
                'preparationTime' => 35,
                'price' => 5.95,
                'image' => 'pikanter_gefluegelwurst_eintopf.jpg',
                'categories' => [
                    CategoryData::POULTRY,
                ],
                'condiments' => [
                    CondimentData::KARTOFFELN => 250,
                    CondimentData::PAPRIKA_ROT => 0.5,
                    CondimentData::KNOBLAUCH => 1,
                    CondimentData::TOMATE => 1,
                    CondimentData::KIDNEYBOHNEN => 1,
                    CondimentData::JOGHURT => 100,
                    CondimentData::PORREE => 0.5,
                    CondimentData::TOMATENPOLPA => 0.5,
                    CondimentData::SCHNITTLAUCH => 5,
                ],
            ],

            [
                'name' => 'Parmesan-Puten-Goujons',
                'subtitle' => 'mit Karotten-Kartoffelstampf und Thymian-Tomatensalat',
                'description' => 'Mal was anderes probieren? Kein Problem, die leckeren Parmesan-Puten-Goujons sind etwas für echte Feinschmecker! Mit dem Tomatensalat bekommt das Gericht einen Frischekick!',
                'difficultLevel' => 2,
                'preparationTime' => 30,
                'price' => 3.25,
                'image' => 'parmesan_puten_goujons.jpg',
                'categories' => [
                    CategoryData::POULTRY,
                ],
                'condiments' => [
                    CondimentData::PUTENBRUST => 125,
                    CondimentData::PARMESAN => 10,
                    CondimentData::SEMMELBROESEL => 35,
                    CondimentData::KIRSCHTOMATEN => 100,
                    CondimentData::THYMIAN => 5,
                    CondimentData::KNOBLAUCH => 1,
                    CondimentData::KAROTTEN => 1,
                    CondimentData::KARTOFFELN => 150,
                    CondimentData::MILCH => 80,
                    CondimentData::ZITRONE => 0.5,
                ],
            ],

            [
                'name' => 'Krosse Putenschnitzel',
                'subtitle' => 'mit knusprigen Kartoffel-Wedges, Schalotten-Stangenbohnen und Dill-Schmand-Soße',
                'description' => 'Dieses Gericht ist zwar etwas anspruchsvoller – aber es lohnt sich! Die knusprigen Kartoffel-Wedges sind ein toller Ersatz für',
                'difficultLevel' => 3,
                'preparationTime' => 30,
                'price' => 4.6,

                'image' => 'krosse_putenschnitzel.jpg',
                'categories' => [
                    CategoryData::POULTRY,
                ],
                'condiments' => [
                    CondimentData::PUTENBRUST => 125,
                    CondimentData::SEMMELBROESEL => 50,
                    CondimentData::ZITRONE => 0.5,
                    CondimentData::STANGENBOHNEN => 80,
                    CondimentData::KARTOFFELN => 250,
                    CondimentData::DILL => 5,
                    CondimentData::SCHALOTTE => 1,
                ],
            ],
        ];

        foreach ($meals as $mealData) {
            $meal = new Meal();
            $meal->setName($mealData['name']);
            $meal->setSubtitle($mealData['subtitle']);
            $meal->setDescription($mealData['description']);
            $meal->setPrice($mealData['price']);
            $meal->setDifficultLevel($mealData['difficultLevel']);
            $meal->setPreparationTime($mealData['preparationTime']);
            $meal->setImagePath(self::IMAGE_PATH.$mealData['image']);

            foreach ($mealData['categories'] as $categoryName) {
                $meal->addCategory($this->getReference($categoryName));
            }

            foreach ($mealData['condiments'] as $condimentName => $quantity) {
                $meal->addCondiment($this->getReference($condimentName), $quantity);
            }

            $manager->persist($meal);
            $manager->flush();
        }
    }

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     * @return int
     */
    public function getOrder()
    {
        return 3;
    }

}