<?php

namespace EasyCooking\FixturesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EasyCooking\SalesFloorBundle\Entity\Condiment;
use EasyCooking\SalesFloorBundle\Entity\Stock;

/**
 * Class StockData
 * @package EasyCooking\FixturesBundle\DataFixtures\ORM
 * @author Marcel Meyer <marcel@devdu.de>
 */
class StockData extends AbstractFixture implements OrderedFixtureInterface
{

    const STOCK_MAX = 35;
    const STOCK_MIN = 10;

    protected $stockMap = [
        'g' => [100, 5000],
    ];

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     * @param \Doctrine\Common\Persistence\ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $condiments = [
            CondimentData::SCHWEINEFILET,
            CondimentData::SCHINKENSPECK,
            CondimentData::WIRSING,
            CondimentData::SCHALOTTE,
            CondimentData::KNOBLAUCH,
            CondimentData::ROSMARIN,
            CondimentData::CREME_FRAICHE,
            CondimentData::SPAETZLE,
            CondimentData::SCHNITTLAUCH,
            CondimentData::RINDERHUEFTSTEAKS,
            CondimentData::KARTOFFELN,
            CondimentData::ZWIEBEL,
            CondimentData::CLEMENTINE,
            CondimentData::KAROTTEN,
            CondimentData::OREGANO,
            CondimentData::FUSILLI_BUCATI,
            CondimentData::HOKKAIDOKUERBIS,
            CondimentData::PECORINO,
            CondimentData::MOZZARELLA,
            CondimentData::SALBEI,
            CondimentData::PETERSILIE,
            CondimentData::QUARK,
            CondimentData::HAEHNCHENBRUST,
            CondimentData::BASMATIREIS,
            CondimentData::ERBSEN,
            CondimentData::ZUCCHINI,
            CondimentData::JOGHURT,
            CondimentData::WHEATY_VEGAN_DOENER,
            CondimentData::PITA_TASCHEN,
            CondimentData::TOMATE,
            CondimentData::SALATHERZEN,
            CondimentData::ZITRONE,
            CondimentData::GURKE,
            CondimentData::VEGAN_CHORIZO,
            CondimentData::SUESSKARTOFFEL,
            CondimentData::SCHWARZE_BOHNEN,
            CondimentData::VOLLKORNSPAGHETTI,
            CondimentData::TOFU,
            CondimentData::PIZZA_TOMATEN,
            CondimentData::COUSCOUS,
            CondimentData::RUCOLA,
            CondimentData::CHILI,
            CondimentData::INGWER,
            CondimentData::KICHERERBSEN,
            CondimentData::AUBERGINE,
            CondimentData::LIMETTE,
            CondimentData::WALNUESSE,
            CondimentData::KORIANDER,
            CondimentData::THUNFISCH,
            CondimentData::SEMMELBROESEL,
            CondimentData::CAYENNEPFEFFER,
            CondimentData::SESAM_BUNS,
            CondimentData::MAYONNAISE,
            CondimentData::DILL,
            CondimentData::STREMELLACHS,
            CondimentData::FRISCHKAESE,
            CondimentData::PIZZATEIG,
            CondimentData::PENNE,
            CondimentData::PARMESAN,
            CondimentData::KIRSCHTOMATEN,
            CondimentData::MANDELN,
            CondimentData::TOMATEN_GETROCKNET,
            CondimentData::SPAGHETTI,
            CondimentData::KUERBISKERNE,
            CondimentData::MEERRETTICH,
            CondimentData::RAEUCHERLACHS,
            CondimentData::PAPRIKA_GELB,
            CondimentData::BASILIKUM,
            CondimentData::HARTKAESE_GERIEBEN,
            CondimentData::CHILIFLOCKEN,
            CondimentData::SAFTORANGE,
            CondimentData::SALATMIX,
            CondimentData::MUSKATNUSS,
            CondimentData::SAHNE,
            CondimentData::NAAN_BROTE,
            CondimentData::BUTTERNUSS_KUERBIS,
            CondimentData::PAPRIKA_ROT,
            CondimentData::TIKKA_MASALA_PASTE,
            CondimentData::TOMATENMARK,
            CondimentData::KOKOSMILCH,
            CondimentData::CHILISCHOTE_ROT,
            CondimentData::BLAETTERTEIG,
            CondimentData::PROSCIUTTO,
            CondimentData::RICOTTA,
            CondimentData::RAEUCHERTOFU,
            CondimentData::HEFEFLOCKEN,
            CondimentData::SPEISESTAERKE,
            CondimentData::KURKUMA,
            CondimentData::SOJASAHNE,
            CondimentData::FETTUCCINE,
            CondimentData::FETA,
            CondimentData::THYMIAN,
            CondimentData::CHICOREE,
            CondimentData::BACON,
            CondimentData::CORNFLAKES,
            CondimentData::GOUDA_GERIEBEN,
            CondimentData::KIDNEYBOHNEN,
            CondimentData::PORREE,
            CondimentData::TOMATENPOLPA,
            CondimentData::PUTENBRUST,
            CondimentData::MILCH,
            CondimentData::STANGENBOHNEN,
            CondimentData::SCHMAND,
        ];

        foreach ($condiments as $condimentName) {
            /**
             * @var $condiment Condiment
             */
            $condiment = $this->getReference($condimentName);

            $stock = new Stock();
            $stock->setCondiment($condiment);
            $stock->setStock($this->getStock($condiment->getUnit()));

            $manager->persist($stock);
            $manager->flush();
        }
    }

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     * @param $unit
     * @return int
     */
    protected function getStock($unit)
    {
        if (isset($this->stockMap[$unit])) {
            return rand($this->stockMap[$unit][0], $this->stockMap[$unit][1]);
        }

        return rand(self::STOCK_MIN, self::STOCK_MAX);
    }

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     * @return int
     */
    public function getOrder()
    {
        return 5;
    }

}