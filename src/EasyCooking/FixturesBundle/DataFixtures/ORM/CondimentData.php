<?php

namespace EasyCooking\FixturesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EasyCooking\SalesFloorBundle\Entity\Condiment;

/**
 * Class CondimentData
 * @package EasyCooking\FixturesBundle\DataFixtures\ORM
 * @author Marcel Meyer <marcel@devdu.de>
 */
class CondimentData extends AbstractFixture implements OrderedFixtureInterface
{

    const SCHWEINEFILET = 'condiment.schweinefilet';
    const SCHINKENSPECK = 'condiment.schinkenspeck';
    const WIRSING = 'condiment.wirsing';
    const SCHALOTTE = 'condiment.schalotte';
    const KNOBLAUCH = 'condiment.knoblauch';
    const ROSMARIN = 'condiment.rosmarin';
    const CREME_FRAICHE = 'condiment.creme_fraiche';
    const SPAETZLE = 'condiment.spaetzle';
    const SCHNITTLAUCH = 'condiment.schnittlauch';
    const RINDERHUEFTSTEAKS = 'condiment.rinderhueftsteaks';
    const KARTOFFELN = 'condiment.kartoffeln';
    const ZWIEBEL = 'condiment.zwiebel';
    const CLEMENTINE = 'condiment.clementine';
    const KAROTTEN = 'condiment.karotten';
    const OREGANO = 'condiment.oregano';
    const FUSILLI_BUCATI = "condiment.fusilli_bucati";
    const HOKKAIDOKUERBIS = "condiment.hokkaidokuerbis";
    const PECORINO = "condiment.pecorino";
    const MOZZARELLA = "condiment.mozzarella";
    const SALBEI = "condiment.salbei";
    const PETERSILIE = "condiment.petersilie";
    const QUARK = "condiment.quark";
    const HAEHNCHENBRUST = "condiment.haehnchenbrust";
    const BASMATIREIS = "condiment.basmatireis";
    const ERBSEN = "condiment.erbsen";
    const ZUCCHINI = "condiment.zucchini";
    const JOGHURT = "condiment.joghurt";
    const WHEATY_VEGAN_DOENER = "condiment.wheaty_vegan_doener";
    const PITA_TASCHEN = "condiment.pita_taschen";
    const TOMATE = "condiment.tomate";
    const SALATHERZEN = "condiment.salatherzen";
    const ZITRONE = "condiment.zitrone";
    const GURKE = "condiment.gurke";
    const VEGAN_CHORIZO = "condiment.vegan_chorizo";
    const SUESSKARTOFFEL = "condiment.sueßkartoffel";
    const SCHWARZE_BOHNEN = "condiment.schwarze_bohnen";
    const VOLLKORNSPAGHETTI = "condiment.vollkornspaghetti";
    const TOFU = "condiment.tofu";
    const PIZZA_TOMATEN = "condiment.pizza_tomaten";
    const COUSCOUS = "condiment.couscous";
    const RUCOLA = "condiment.rucola";
    const CHILI = "condiment.chili";
    const INGWER = "condiment.ingwer";
    const KICHERERBSEN = "condiment.kichererbsen";
    const AUBERGINE = "condiment.aubergine";
    const LIMETTE = "condiment.limette";
    const WALNUESSE = "condiment.walnuesse";
    const KORIANDER = "condiment.koriander";
    const THUNFISCH = "condiment.thunfisch";
    const SEMMELBROESEL = "condiment.semmelbroesel";
    const CAYENNEPFEFFER = "condiment.cayennepfeffer";
    const SESAM_BUNS = "condiment.sesam_buns";
    const MAYONNAISE = "condiment.mayonnaise";
    const DILL = "condiment.dill";
    const STREMELLACHS = "condiment.stremellachs";
    const FRISCHKAESE = "condiment.frischkaese";
    const PIZZATEIG = "condiment.pizzateig";
    const PENNE = "condiment.penne";
    const PARMESAN = "condiment.parmesan";
    const KIRSCHTOMATEN = "condiment.kirschtomaten";
    const MANDELN = "condiment.mandeln";
    const TOMATEN_GETROCKNET = "condiment.tomaten_getrocknet";
    const SPAGHETTI = "condiment.spaghetti";
    const KUERBISKERNE = "condiment.kuerbiskerne";
    const MEERRETTICH = "condiment.meerrettich";
    const RAEUCHERLACHS = "condiment.raeucherlachs";
    const PAPRIKA_GELB = "condiment.paprika_gelb";
    const BASILIKUM = "condiment.basilikum";
    const HARTKAESE_GERIEBEN = "condiment.hartkaese_gerieben";
    const CHILIFLOCKEN = "condiment.chiliflocken";
    const SAFTORANGE = "condiment.saftorange";
    const SALATMIX = "condiment.salatmix";
    const MUSKATNUSS = "condiment.muskatnuss";
    const SAHNE = "condiment.sahne";
    const NAAN_BROTE = "condiment.naan_brote";
    const BUTTERNUSS_KUERBIS = "condiment.butternuss_kuerbis";
    const PAPRIKA_ROT = "condiment.paprika_rot";
    const TIKKA_MASALA_PASTE = "condiment.tikka_masala_paste";
    const TOMATENMARK = "condiment.tomatenmark";
    const KOKOSMILCH = "condiment.kokosmilch";
    const CHILISCHOTE_ROT = "condiment.chilischote_rot";
    const BLAETTERTEIG = "condiment.blaetterteig";
    const PROSCIUTTO = "condiment.prosciutto";
    const RICOTTA = "condiment.ricotta";
    const RAEUCHERTOFU = "condiment.raeuchertofu";
    const HEFEFLOCKEN = "condiment.hefeflocken";
    const SPEISESTAERKE = "condiment.speisestaerke";
    const KURKUMA = "condiment.kurkuma";
    const SOJASAHNE = "condiment.sojasahne";
    const FETTUCCINE = "condiment.fettuccine";
    const FETA = "condiment.feta";
    const THYMIAN = "condiment.thymian";
    const CHICOREE = "condiment.chicoree";
    const BACON = "condiment.bacon";
    const CORNFLAKES = "condiment.cornflakes";
    const GOUDA_GERIEBEN = "condiment.gouda_gerieben";
    const KIDNEYBOHNEN = "condiment.kidneybohnen";
    const PORREE = "condiment.porree";
    const TOMATENPOLPA = "condiment.tomatenpolpa";
    const PUTENBRUST = "condiment.putenbrust";
    const MILCH = "condiment.milch";
    const STANGENBOHNEN = "condiment.stangenbohnen";
    const SCHMAND = "condiment.schmand";

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     * @param \Doctrine\Common\Persistence\ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $condiments = [
            ['name' => 'Schweinefilet', 'ref' => self::SCHWEINEFILET, 'unit' => 'g'],
            ['name' => 'Schinkenspeck', 'ref' => self::SCHINKENSPECK, 'unit' => 'g'],
            ['name' => 'Wirsing', 'ref' => self::WIRSING, 'unit' => null],
            ['name' => 'Schalotte', 'ref' => self::SCHALOTTE, 'unit' => null],
            ['name' => 'Knoblauch', 'ref' => self::KNOBLAUCH, 'unit' => null],
            ['name' => 'Rosmarin', 'ref' => self::ROSMARIN, 'unit' => null],
            ['name' => 'Crème fraîche', 'ref' => self::CREME_FRAICHE, 'unit' => 'g'],
            ['name' => 'Spätzle', 'ref' => self::SPAETZLE, 'unit' => 'g'],
            ['name' => 'Schnittlauch', 'ref' => self::SCHNITTLAUCH, 'unit' => 'g'],
            ['name' => 'Rinderhüftsteaks', 'ref' => self::RINDERHUEFTSTEAKS, 'unit' => 'g'],
            ['name' => 'Kartoffeln', 'ref' => self::KARTOFFELN, 'unit' => 'g'],
            ['name' => 'Zwiebel', 'ref' => self::ZWIEBEL, 'unit' => null],
            ['name' => 'Clementine', 'ref' => self::CLEMENTINE, 'unit' => null],
            ['name' => 'Karotten', 'ref' => self::KAROTTEN, 'unit' => null],
            ['name' => 'Oregano', 'ref' => self::OREGANO, 'unit' => 'g'],
            ['name' => 'Fusilli Bucati', 'ref' => self::FUSILLI_BUCATI, 'unit' => 'g'],
            ['name' => 'Hokkaidokürbis', 'ref' => self::HOKKAIDOKUERBIS, 'unit' => null],
            ['name' => 'Pecorino', 'ref' => self::PECORINO, 'unit' => 'g'],
            ['name' => 'Mozzarella', 'ref' => self::MOZZARELLA, 'unit' => 'g'],
            ['name' => 'Salbei', 'ref' => self::SALBEI, 'unit' => 'g'],
            ['name' => 'Petersilie', 'ref' => self::PETERSILIE, 'unit' => 'g'],
            ['name' => 'Quark', 'ref' => self::QUARK, 'unit' => 'g'],
            ['name' => 'Hähnchenbrust', 'ref' => self::HAEHNCHENBRUST, 'unit' => 'g'],
            ['name' => 'Basmatireis', 'ref' => self::BASMATIREIS, 'unit' => 'g'],
            ['name' => 'Erbsen', 'ref' => self::ERBSEN, 'unit' => 'g'],
            ['name' => 'Zucchini', 'ref' => self::ZUCCHINI, 'unit' => null],
            ['name' => 'Joghurt', 'ref' => self::JOGHURT, 'unit' => 'g'],
            ['name' => 'Wheaty Vegan Döner', 'ref' => self::WHEATY_VEGAN_DOENER, 'unit' => 'g'],
            ['name' => 'Pita Taschen', 'ref' => self::PITA_TASCHEN, 'unit' => null],
            ['name' => 'Tomate', 'ref' => self::TOMATE, 'unit' => null],
            ['name' => 'Salatherzen', 'ref' => self::SALATHERZEN, 'unit' => null],
            ['name' => 'Zitrone', 'ref' => self::ZITRONE, 'unit' => null],
            ['name' => 'Gurke', 'ref' => self::GURKE, 'unit' => null],
            ['name' => 'Vegan Chorizo', 'ref' => self::VEGAN_CHORIZO, 'unit' => 'g'],
            ['name' => 'Süßkartoffel', 'ref' => self::SUESSKARTOFFEL, 'unit' => 'g'],
            ['name' => 'Schwarze Bohnen', 'ref' => self::SCHWARZE_BOHNEN, 'unit' => 'Dose'],
            ['name' => 'Vollkornspaghetti', 'ref' => self::VOLLKORNSPAGHETTI, 'unit' => 'g'],
            ['name' => 'Tofu', 'ref' => self::TOFU, 'unit' => 'g'],
            ['name' => 'Pizza-Tomaten', 'ref' => self::PIZZA_TOMATEN, 'unit' => 'Dose'],
            ['name' => 'Couscous', 'ref' => self::COUSCOUS, 'unit' => 'g'],
            ['name' => 'Rucola', 'ref' => self::RUCOLA, 'unit' => 'g'],
            ['name' => 'Chili', 'ref' => self::CHILI, 'unit' => null],
            ['name' => 'Ingwer', 'ref' => self::INGWER, 'unit' => 'g'],
            ['name' => 'Kichererbsen', 'ref' => self::KICHERERBSEN, 'unit' => 'Dose'],
            ['name' => 'Aubergine', 'ref' => self::AUBERGINE, 'unit' => null],
            ['name' => 'Limette', 'ref' => self::LIMETTE, 'unit' => null],
            ['name' => 'Walnüsse', 'ref' => self::WALNUESSE, 'unit' => 'g'],
            ['name' => 'Koriander', 'ref' => self::KORIANDER, 'unit' => 'g'],
            ['name' => 'Thunfisch', 'ref' => self::THUNFISCH, 'unit' => 'Dose'],
            ['name' => 'Semmelbrösel', 'ref' => self::SEMMELBROESEL, 'unit' => 'g'],
            ['name' => 'Cayennepfeffer', 'ref' => self::CAYENNEPFEFFER, 'unit' => 'g'],
            ['name' => 'Sesam Buns', 'ref' => self::SESAM_BUNS, 'unit' => null],
            ['name' => 'Mayonnaise', 'ref' => self::MAYONNAISE, 'unit' => 'g'],
            ['name' => 'Stremellachs', 'ref' => self::STREMELLACHS, 'unit' => 'g'],
            ['name' => 'Frischkäse', 'ref' => self::FRISCHKAESE, 'unit' => 'g'],
            ['name' => 'Pizzateig', 'ref' => self::PIZZATEIG, 'unit' => null],
            ['name' => 'Penne', 'ref' => self::PENNE, 'unit' => 'g'],
            ['name' => 'Parmesan', 'ref' => self::PARMESAN, 'unit' => 'g'],
            ['name' => 'Kirschtomaten', 'ref' => self::KIRSCHTOMATEN, 'unit' => 'g'],
            ['name' => 'Mandeln', 'ref' => self::MANDELN, 'unit' => 'g'],
            ['name' => 'Tomaten, getrocknet', 'ref' => self::TOMATEN_GETROCKNET, 'unit' => 'g'],
            ['name' => 'Spaghetti', 'ref' => self::SPAGHETTI, 'unit' => 'g'],
            ['name' => 'Kürbiskerne', 'ref' => self::KUERBISKERNE, 'unit' => 'g'],
            ['name' => 'Meerrettich', 'ref' => self::MEERRETTICH, 'unit' => 'g'],
            ['name' => 'Räucherlachs', 'ref' => self::RAEUCHERLACHS, 'unit' => 'g'],
            ['name' => 'gelbe Paprika', 'ref' => self::PAPRIKA_GELB, 'unit' => null],
            ['name' => 'Basilikum', 'ref' => self::BASILIKUM, 'unit' => 'g'],
            ['name' => 'Hartkäse, gerieben', 'ref' => self::HARTKAESE_GERIEBEN, 'unit' => 'g'],
            ['name' => 'Chiliflocken', 'ref' => self::CHILIFLOCKEN, 'unit' => 'g'],
            ['name' => 'Saftorange', 'ref' => self::SAFTORANGE, 'unit' => null],
            ['name' => 'Salatmix', 'ref' => self::SALATMIX, 'unit' => 'g'],
            ['name' => 'Muskatnuss', 'ref' => self::MUSKATNUSS, 'unit' => 'g'],
            ['name' => 'Sahne', 'ref' => self::SAHNE, 'unit' => 'g'],
            ['name' => 'Naan-Brote', 'ref' => self::NAAN_BROTE, 'unit' => null],
            ['name' => 'Butternuss-Kürbis', 'ref' => self::BUTTERNUSS_KUERBIS, 'unit' => null],
            ['name' => 'Paprika, rot', 'ref' => self::PAPRIKA_ROT, 'unit' => null],
            ['name' => 'Tikka-Masala-Paste', 'ref' => self::TIKKA_MASALA_PASTE, 'unit' => 'g'],
            ['name' => 'Tomatenmark', 'ref' => self::TOMATENMARK, 'unit' => 'g'],
            ['name' => 'Kokosmilch', 'ref' => self::KOKOSMILCH, 'unit' => 'ml'],
            ['name' => 'Chilischote, rot', 'ref' => self::CHILISCHOTE_ROT, 'unit' => null],
            ['name' => 'Blätterteig', 'ref' => self::BLAETTERTEIG, 'unit' => null],
            ['name' => 'Prosciutto', 'ref' => self::PROSCIUTTO, 'unit' => 'Scheiben'],
            ['name' => 'Ricotta', 'ref' => self::RICOTTA, 'unit' => 'g'],
            ['name' => 'Räuchertofu', 'ref' => self::RAEUCHERTOFU, 'unit' => 'g'],
            ['name' => 'Hefeflocken', 'ref' => self::HEFEFLOCKEN, 'unit' => 'g'],
            ['name' => 'Speisestärke', 'ref' => self::SPEISESTAERKE, 'unit' => 'g'],
            ['name' => 'Kurkuma', 'ref' => self::KURKUMA, 'unit' => 'g'],
            ['name' => 'Sojasahne', 'ref' => self::SOJASAHNE, 'unit' => 'g'],
            ['name' => 'Fettuccine', 'ref' => self::FETTUCCINE, 'unit' => 'g'],
            ['name' => 'Feta', 'ref' => self::FETA, 'unit' => 'g'],
            ['name' => 'Thymian', 'ref' => self::THYMIAN, 'unit' => 'g'],
            ['name' => 'Chicorée', 'ref' => self::CHICOREE, 'unit' => null],
            ['name' => 'Bacon', 'ref' => self::BACON, 'unit' => 'g'],
            ['name' => 'Cornflakes', 'ref' => self::CORNFLAKES, 'unit' => 'g'],
            ['name' => 'Gouda, gerieben', 'ref' => self::GOUDA_GERIEBEN, 'unit' => 'g'],
            ['name' => 'Kidneybohnen', 'ref' => self::KIDNEYBOHNEN, 'unit' => 'Dose'],
            ['name' => 'Porree', 'ref' => self::PORREE, 'unit' => null],
            ['name' => 'Tomatenpolpa', 'ref' => self::TOMATENPOLPA, 'unit' => 'Dose'],
            ['name' => 'Putenbrust', 'ref' => self::PUTENBRUST, 'unit' => 'g'],
            ['name' => 'Milch', 'ref' => self::MILCH, 'unit' => 'ml'],
            ['name' => 'Stangenbohnen', 'ref' => self::STANGENBOHNEN, 'unit' => 'g'],
            ['name' => 'Schmand', 'ref' => self::SCHMAND, 'unit' => 'g'],
            ['name' => 'Dil', 'ref' => self::DILL, 'unit' => 'g'],
        ];

        foreach ($condiments as $condimentData) {
            $condiment = new Condiment();
            $condiment->setName($condimentData['name']);
            $condiment->setUnit($condimentData['unit']);

            $manager->persist($condiment);
            $manager->flush();

            $this->addReference($condimentData['ref'], $condiment);
        }
    }

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     * @return int
     */
    public function getOrder()
    {
        return 2;
    }

}