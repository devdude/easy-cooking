<?php

namespace EasyCooking\FixturesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EasyCooking\SalesFloorBundle\Entity\Category;

/**
 * Class CategoryData
 * @package EasyCooking\FixturesBundle\DataFixtures\ORM
 * @author Marcel Meyer <marcel@devdu.de>
 */
class CategoryData extends AbstractFixture implements OrderedFixtureInterface
{

    const MEAT = 'category.meat';
    const FISH = 'category.fish';
    const POULTRY = 'category.poultry';
    const VEGAN = 'category.vegan';
    const VEGGIE = 'category.veggie';

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     * @param \Doctrine\Common\Persistence\ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $categories = [
            [
                'name' => 'Fleisch',
                'ref' => self::MEAT,
            ],
            [
                'name' => 'Fisch',
                'ref' => self::FISH,
            ],
            [
                'name' => 'Geflügel',
                'ref' => self::POULTRY,
            ],
            [
                'name' => 'Vegan',
                'ref' => self::VEGAN,
            ],
            [
                'name' => 'Vegetarisch',
                'ref' => self::VEGGIE,
            ],
        ];

        foreach ($categories as $categoryData) {
            $category = new Category();
            $category->setName($categoryData['name']);

            $manager->persist($category);
            $manager->flush();

            $this->addReference($categoryData['ref'], $category);
        }
    }

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     * @return int
     */
    public function getOrder()
    {
        return 1;
    }

}