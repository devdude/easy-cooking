<?php

namespace EasyCooking\FixturesBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class EasyCookingFixturesBundle
 * @package EasyCooking\FixturesBundle
 * @author Marcel Meyer <marcel@devdu.de>
 */
class EasyCookingFixturesBundle extends Bundle
{

}