<?php

namespace EasyCooking\SalesFloorBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class MainMenuBuilder
 * @package EasyCooking\SalesFloorBundle\Menu
 * @author Marcel Meyer <marcel@devdu.de>
 */
class MainMenuBuilder implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     * @param \Knp\Menu\FactoryInterface $factory
     * @return \Knp\Menu\ItemInterface
     */
    public function mainMenu(FactoryInterface $factory)
    {
        $menu = $factory->createItem(
            'root',
            [
                'childrenAttributes' => [
                    'class' => 'menu',
                ],
            ]
        );

        $menu->addChild(
            'Übersicht',
            [
                'route' => 'frontpage',
            ]
        );

        return $menu;
    }
}