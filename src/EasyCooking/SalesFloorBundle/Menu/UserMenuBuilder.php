<?php

namespace EasyCooking\SalesFloorBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class UserMenuBuilder
 * @package EasyCooking\SalesFloorBundle\Menu
 * @author Marcel Meyer <marcel@devdu.de>
 */
class UserMenuBuilder implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     * @param \Knp\Menu\FactoryInterface $factory
     * @return \Knp\Menu\ItemInterface
     */
    public function userMenu(FactoryInterface $factory)
    {
        $router = $this->container->get('router');

        $menu = $factory->createItem(
            'root',
            [
                'childrenAttributes' => [
                    'class' => 'menu',
                ],
            ]
        );

        $menu->addChild(
            'basket',
            [
                'href' => false,
                'label' => false,
                'attributes' => [
                    'class' => ' jsMiniBasket',
                    //'data-basket-url' => $router->generate('get_basket_count'),
                    //'data-checkout-url' => $router->generate('checkout_address'),
                ],
            ]
        );

        return $menu;
    }

}