<?php

namespace EasyCooking\SalesFloorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class MealHasCondiment
 * @package EasyCooking\SalesFloorBundle\Entity
 * @author Marcel Meyer <marcel@devdu.de>
 *
 * @ORM\Entity()
 * @ORM\Table(
 *     name="meal_condiments"
 * )
 */
class MealHasCondiment implements \JsonSerializable
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Condiment
     * @ORM\ManyToOne(targetEntity="Condiment", inversedBy="meals")
     * @ORM\JoinColumn(name="condiment_id")
     */
    protected $condiment;

    /**
     * @var Meal
     * @ORM\ManyToOne(targetEntity="Meal", inversedBy="condiments")
     * @ORM\JoinColumn(name="meal_id")
     */
    protected $meal;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    protected $quantity;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Condiment
     */
    public function getCondiment()
    {
        return $this->condiment;
    }

    /**
     * @param Condiment $condiment
     */
    public function setCondiment($condiment)
    {
        $this->condiment = $condiment;
    }

    /**
     * @return Meal
     */
    public function getMeal()
    {
        return $this->meal;
    }

    /**
     * @param Meal $meal
     */
    public function setMeal($meal)
    {
        $this->meal = $meal;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     * @return array
     */
    function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'condiment' => $this->getCondiment()->jsonSerialize(),
            'quantity' => $this->getQuantity(),
        ];
    }

}