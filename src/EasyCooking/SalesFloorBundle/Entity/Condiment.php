<?php

namespace EasyCooking\SalesFloorBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Condiment
 * @package EasyCooking\SalesFloorBundle\Entity
 * @author Marcel Meyer <marcel@devdu.de>
 *
 * @ORM\Entity(
 *     repositoryClass="EasyCooking\SalesFloorBundle\Repository\CondimentRepository"
 * )
 * @ORM\Table(
 *     name="condiments"
 * )
 */
class Condiment implements \JsonSerializable
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="MealHasCondiment", mappedBy="condiment", cascade={"all"})
     */
    protected $meals;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $unit;

    /**
     * @var Stock
     * @ORM\OneToOne(targetEntity="Stock", mappedBy="condiment", cascade={"all"})
     */
    protected $stock;

    /**
     * Condiment constructor.
     */
    public function __construct()
    {
        $this->meals = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return ArrayCollection
     */
    public function getMeals()
    {
        return $this->meals;
    }

    /**
     * @param ArrayCollection $meals
     */
    public function setMeals($meals)
    {
        $this->meals = $meals;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param string $unit
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
    }

    /**
     * @return Stock
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param Stock $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     * @return array
     */
    function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'unit' => $this->getUnit(),
            'stock' => $this->getStock()->jsonSerialize(),
        ];
    }

}