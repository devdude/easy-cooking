<?php

namespace EasyCooking\SalesFloorBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class Meal
 * @package EasyCooking\SalesFloorBundle\Entity
 * @author Marcel Meyer <marcel@devdu.de>
 *
 * @ORM\Entity(
 *     repositoryClass="EasyCooking\SalesFloorBundle\Repository\MealRepository"
 * )
 * @ORM\Table(
 *     name="meals"
 * )
 */
class Meal
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="MealHasCondiment", mappedBy="meal", cascade={"all"})
     */
    protected $condiments;

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @var string
     * @Gedmo\Slug(fields={"name"}, unique=true)
     * @ORM\Column(type="string", length=255)
     */
    protected $slug;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    protected $subtitle;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $description;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    protected $price;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $difficultLevel;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $preparationTime;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    protected $imagePath;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="meals")
     * @ORM\JoinTable(name="meals_categories")
     */
    protected $categories;

    /**
     * Meal constructor.
     */
    public function __construct()
    {
        $this->condiments = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     * @param \EasyCooking\SalesFloorBundle\Entity\Condiment $condiment
     * @param $quantity
     */
    public function addCondiment(Condiment $condiment, $quantity)
    {
        $mealCondiment = new MealHasCondiment();
        $mealCondiment->setCondiment($condiment);
        $mealCondiment->setMeal($this);
        $mealCondiment->setQuantity($quantity);

        $this->getCondiments()->add($mealCondiment);
    }

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     * @param \EasyCooking\SalesFloorBundle\Entity\Category $category
     */
    public function addCategory(Category $category) {
        $this->getCategories()->add($category);
    }
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return ArrayCollection
     */
    public function getCondiments()
    {
        return $this->condiments;
    }

    /**
     * @param ArrayCollection $condiments
     */
    public function setCondiments($condiments)
    {
        $this->condiments = $condiments;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getDifficultLevel()
    {
        return $this->difficultLevel;
    }

    /**
     * @param int $difficultLevel
     */
    public function setDifficultLevel($difficultLevel)
    {
        $this->difficultLevel = $difficultLevel;
    }

    /**
     * @return int
     */
    public function getPreparationTime()
    {
        return $this->preparationTime;
    }

    /**
     * @param int $preparationTime
     */
    public function setPreparationTime($preparationTime)
    {
        $this->preparationTime = $preparationTime;
    }

    /**
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * @param string $imagePath
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;
    }

    /**
     * @return ArrayCollection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param ArrayCollection $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

}