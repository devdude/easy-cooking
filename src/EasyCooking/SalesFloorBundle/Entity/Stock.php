<?php

namespace EasyCooking\SalesFloorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Stock
 * @package EasyCooking\SalesFloorBundle\Entity
 * @author Marcel Meyer <marcel@devdu.de>
 *
 * @ORM\Entity(
 *     repositoryClass="EasyCooking\SalesFloorBundle\Repository\StockRepository"
 * )
 * @ORM\Table(
 *     name="stocks"
 * )
 */
class Stock implements \JsonSerializable
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Condiment
     * @ORM\OneToOne(targetEntity="Condiment", inversedBy="stock")
     * @ORM\JoinColumn(name="condiment_id")
     */
    protected $condiment;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $stock = 0;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $reserved = 0;

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     * @return int
     */
    public function getAvailableStock()
    {
        return $this->getStock() - $this->getReserved();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Condiment
     */
    public function getCondiment()
    {
        return $this->condiment;
    }

    /**
     * @param Condiment $condiment
     */
    public function setCondiment($condiment)
    {
        $this->condiment = $condiment;
    }

    /**
     * @return int
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return int
     */
    public function getReserved()
    {
        return $this->reserved;
    }

    /**
     * @param int $reserved
     */
    public function setReserved($reserved)
    {
        $this->reserved = $reserved;
    }

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     * @return array
     */
    function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'stock' => $this->getStock(),
            'reserved' => $this->getReserved(),
        ];
    }

}