<?php

namespace EasyCooking\SalesFloorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class Category
 * @package EasyCooking\SalesFloorBundle\Entity
 * @author Marcel Meyer <marcel@devdu.de>
 *
 * @ORM\Entity(
 *     repositoryClass="EasyCooking\SalesFloorBundle\Repository\CategoryRepository"
 * )
 * @ORM\Table(
 *     name="categories"
 * )
 */
class Category
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=40)
     */
    protected $name;

    /**
     * @var string
     * @Gedmo\Slug(fields={"name"}, unique=true)
     * @ORM\Column(type="string", length=255)
     */
    protected $slug;

    /**
     * @var Meal
     * @ORM\ManyToMany(targetEntity="Meal", mappedBy="categories")
     */
    protected $meals;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return Meal
     */
    public function getMeals()
    {
        return $this->meals;
    }

    /**
     * @param Meal $meals
     */
    public function setMeals($meals)
    {
        $this->meals = $meals;
    }

}