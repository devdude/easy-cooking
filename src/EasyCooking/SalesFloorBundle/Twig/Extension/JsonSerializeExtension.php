<?php

namespace EasyCooking\SalesFloorBundle\Twig\Extension;

use Doctrine\Common\Collections\Collection;

/**
 * Class JsonSerializeExtension
 * @package EasyCooking\SalesFloorBundle\Twig\Extension
 * @author Marcel Meyer <marcel@devdu.de>
 */
class JsonSerializeExtension extends \Twig_Extension
{

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     * @return string
     */
    public function getName()
    {
        return 'ec.json_serialize';
    }

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('json_serialize', [$this, 'serializeJson']),
        ];
    }

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     * @param $object
     * @return null|string
     */
    public function serializeJson($object)
    {
        $result = $this->serializeObject($object);

        if ($result === null) {
            return null;
        }

        return json_encode($result);
    }

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     * @param $object
     * @return array|mixed|null
     */
    protected function serializeObject($object)
    {
        if ($object instanceof \JsonSerializable) {
            return $object->jsonSerialize();
        }

        if ($object instanceof Collection) {
            $result = [];

            foreach ($object as $item) {
                $result[] = $this->serializeObject($item);
            }

            return $result;
        }

        if (is_array($object)) {
            return $object;
        }

        return null;
    }

}