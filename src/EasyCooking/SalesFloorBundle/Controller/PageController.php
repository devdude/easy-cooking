<?php

namespace EasyCooking\SalesFloorBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Yaml\Parser;

/**
 * Class PageController
 * @package EasyCooking\SalesFloorBundle\Controller
 * @author Marcel Meyer <marcel@devdu.de>
 */
class PageController extends Controller
{

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     *
     * @Route("/impressum", name="imprint")
     * @Template()
     */
    public function imprintAction()
    {
        $ymlParser = new Parser();
        $iconCopyrightsFilePath = $this->get('kernel')->getRootDir().'/../ui/icons/copyright.yml';
        $iconCopyrights = $ymlParser->parse(file_get_contents($iconCopyrightsFilePath));

        return [
            'iconCopyrights' => $iconCopyrights,
        ];
    }

}