<?php

namespace EasyCooking\SalesFloorBundle\Controller;

use EasyCooking\SalesFloorBundle\Repository\MealRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class FrontpageController
 * @package EasyCooking\SalesFloorBundle\Controller
 * @author Marcel Meyer <marcel@devdu.de>
 */
class FrontpageController extends Controller
{

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     *
     * @Route("/", name="frontpage")
     * @Template()
     */
    public function indexAction()
    {
        $meals = $this->get('ec.repository.meal')->findAll();

        return [
            'meals' => $meals,
        ];
    }

}