<?php

namespace EasyCooking\SalesFloorBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class EasyCookingSalesFloorBundle
 * @package EasyCooking\SalesFloorBundle
 * @author Marcel Meyer <marcel@devdu.de>
 */
class EasyCookingSalesFloorBundle extends Bundle
{

}