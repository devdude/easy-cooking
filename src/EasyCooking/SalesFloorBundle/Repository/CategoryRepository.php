<?php

namespace EasyCooking\SalesFloorBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class CategoryRepository
 * @package EasyCooking\SalesFloorBundle\Repository
 * @author Marcel Meyer <marcel@devdu.de>
 */
class CategoryRepository extends EntityRepository
{

    const NAME = 'EasyCookingSalesFloorBundle:Category';

}