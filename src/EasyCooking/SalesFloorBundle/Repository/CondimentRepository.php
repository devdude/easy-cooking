<?php

namespace EasyCooking\SalesFloorBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class CondimentRepository
 * @package EasyCooking\SalesFloorBundle\Repository
 * @author Marcel Meyer <marcel@devdu.de>
 */
class CondimentRepository extends EntityRepository
{

    const NAME = 'EasyCookingSalesFloorBundle:Condiment';

}