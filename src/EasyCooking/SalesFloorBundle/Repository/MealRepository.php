<?php

namespace EasyCooking\SalesFloorBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class MealRepository
 * @package EasyCooking\SalesFloorBundle\Repository
 * @author Marcel Meyer <marcel@devdu.de>
 */
class MealRepository extends EntityRepository
{

    const NAME = 'EasyCookingSalesFloorBundle:Meal';

}