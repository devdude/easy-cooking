<?php

namespace EasyCooking\SalesFloorBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class StockRepository
 * @package EasyCooking\SalesFloorBundle\Repository
 * @author Marcel Meyer <marcel@devdu.de>
 */
class StockRepository extends EntityRepository
{

    const NAME = 'EasyCookingSalesFloorBundle:Stock';

}