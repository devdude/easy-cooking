<?php

namespace EasyCooking\SalesFloorBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Class EasyCookingSalesFloorExtension
 * @package EasyCooking\SalesFloorBundle\DependencyInjection
 * @author Marcel Meyer <marcel@devdu.de>
 */
class EasyCookingSalesFloorExtension extends Extension
{

    /**
     * @author Marcel Meyer <marcel@devdu.de>
     * @param array $configs
     * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('twig_extensions.yml');
        $loader->load('repositories.yml');
    }

}