var React = require('react');
var ReactDOM = require('react-dom');
var jQuery = require('jquery');

var CondimentList = require('./components/CondimentList');
var MiniBasket = require('./components/MiniBasket');

/**
 * Condiment list
 */
$condimentList = jQuery('.jsCondimentList');

if ($condimentList.length) {
    var condiments = $condimentList.data('condiments');
    var mealId = $condimentList.data('meal-id');
    var price = $condimentList.data('price');
    var addToBasketUrl = $condimentList.data('add-to-basket-url');

    ReactDOM.render(
        <CondimentList condiments={condiments} mealId={mealId} price={price} addToBasketUrl={addToBasketUrl}/>,
        $condimentList.get(0)
    );
}

/**
 * MiniBasket
 */
$miniBasket = jQuery('.jsMiniBasket');

if ($miniBasket.length) {
    var basketUrl = $miniBasket.data('basket-url');
    var checkoutUrl = $miniBasket.data('checkout-url');

    ReactDOM.render(
        <MiniBasket basketUrl={basketUrl} checkoutUrl={checkoutUrl}/>,
        $miniBasket.get(0)
    );
}