var React = require('react');

var LoadingIndicator = React.createClass({
    render: function() {
        return (
            <div className="loader">
                <img src="/assets/images/loading.svg"/>
            </div>
        );
    }
});

module.exports = LoadingIndicator;