var React = require('react');
var jQuery = require('jquery');

var EventEmitter = require('./../event/EventEmitter');
var EventConstants = require('./../event/Constants');

var CondimentCounter = require('./CondimentCounter');
var LoadingIndicator = require('./LoadingIndicator');

var DISABLED_TIME = 4000;

var CondimentList = React.createClass({
    getInitialState: function () {
        return {
            quantity: 1,
            isLoading: false,
            isDisabled: false,
            isOutOfStock: false
        };
    },

    handleQuantityChange: function (quantity) {
        this.setState({
            quantity: quantity
        });
    },

    addToBasketHandler: function () {
        if (this.state.isDisabled || this.state.isLoading || this.state.isOutOfStock) {
            return;
        }

        this.setState({
            isLoading: true
        });

        var self = this;
        var addToBasketUrl = this.props.addToBasketUrl;

        if (!addToBasketUrl) {
            this.setState({isLoading: false});
            console.error('No add to basketurl defined.');
            return;
        }

        jQuery.ajax({
            url: addToBasketUrl,
            method: 'post',
            data: {
                mealId: this.props.mealId,
                quantity: this.state.quantity
            },
            success: function (response) {
                self.setState({
                    isLoading: false,
                    isDisabled: true
                });
                
                console.log(response);
                if (response.success) {
                    self.activateAddToBasketButtonDelayed();
                    
                    EventEmitter.emit(EventConstants.ADD_TO_BASKET, {
                        basketItem: response.basketItem,
                        total: response.total
                    });
                } else {
                    self.setState({
                        isOutOfStock: true
                    });
                }
            },
            failure: function (response) {
                console.error(response);
                self.setState({
                    isLoading: false
                });
            }
        });
    },

    activateAddToBasketButtonDelayed: function () {
        var self = this;

        window.setTimeout(function () {
            self.setState({
                isDisabled: false
            });
        }, DISABLED_TIME);
    },

    render: function () {
        var quantity = this.state.quantity;
        var condiments = this.props.condiments;
        var price = this.props.price;
        var isLoading = this.state.isLoading;
        var isDisabled = this.state.isDisabled;
        var isOutOfStock = this.state.isOutOfStock;
        var buttonText = (isOutOfStock ? 'Ausverkauft' : (isDisabled ? 'Hinzugefügt' : 'In den Warenkorb'));

        return (
            <div>
                <h4>Zutaten für <CondimentCounter quantity={quantity} onChanged={this.handleQuantityChange}/></h4>

                <dl className="condiment-list">
                    {condiments.map(function (condiment) {
                        return (
                            <div key={condiment.id}>
                                <dt>{condiment.quantity * quantity}{condiment.condiment.unit}</dt>
                                <dd>{condiment.condiment.name}</dd>
                            </div>
                        );
                    })}
                </dl>

                <div className="price-container">
                    <div className="price">{(price * quantity).toFixed(2).replace('.', ',')}€</div>

                    <button className="button add-to-basket" onClick={this.addToBasketHandler}>
                        {isLoading ? <LoadingIndicator/> : ''} {buttonText}
                    </button>
                </div>
            </div>
        );
    }
});

module.exports = CondimentList;