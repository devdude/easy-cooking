var React = require('react');

var CondimentCounter = React.createClass({
    handleIncrease: function () {
        var quantity = this.props.quantity + 1;
        this.props.onChanged(quantity);
    },

    handleDecrease: function () {
        var quantity = this.props.quantity - 1;

        if (quantity <= 0) {
            quantity = 1;
        }

        this.props.onChanged(quantity);
    },

    render: function () {
        var quantity = this.props.quantity;
        var person = quantity + ' Person';

        if (quantity > 1) {
            person = quantity + ' Personen';
        }

        return (
            <span>
                <span>{person}</span>
                <button className="button tiny quantity" onClick={this.handleIncrease}>+</button>
                <button className="button tiny quantity" onClick={this.handleDecrease}>-</button>
            </span>
        );
    }
});

module.exports = CondimentCounter;