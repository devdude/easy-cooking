var React = require('react');
var jQuery = require('jquery');

var EventEmitter = require('./../event/EventEmitter');
var EventConstants = require('./../event/Constants');

var MiniBasket = React.createClass({
    getInitialState: function () {
        return {
            totalItems: 0
        };
    },

    componentDidMount: function () {
        EventEmitter.on(EventConstants.ADD_TO_BASKET, this.updateBasket);

        if (this.props.basketUrl) {
            var self = this;

            jQuery.ajax({
                url: this.props.basketUrl,
                success: function (response) {
                    self.setState({
                        totalItems: response.total
                    });
                }
            });
        }
    },

    updateBasket: function (event) {
        this.setState({
            totalItems: event.total
        });
    },
    
    goToCheckout: function() {
        if(!this.props.checkoutUrl || !this.state.totalItems) {
            return;
        }
        
        window.location.href = this.props.checkoutUrl;
    },

    render: function () {
        var totalItems = this.state.totalItems;

        return (
            <div className="mini-basket">
                <div className="basket-icon" onClick={this.goToCheckout}>
                    <i className="icon-cart"/>
                    {totalItems > 0 ? <span className="counter">{totalItems}</span> : ''}
                </div>
            </div>
        );
    }
});

module.exports = MiniBasket;