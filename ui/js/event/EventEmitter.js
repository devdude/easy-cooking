var EventEmitter2 = require('imports?define=>false!eventemitter2').EventEmitter2;

var eventEmitterOptions = {
    wildcard: true,
    delimiter: '.'
};

module.exports = new EventEmitter2(eventEmitterOptions);