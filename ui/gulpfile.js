var gulp = require('gulp');
var gutil = require('gulp-util');
var webpack = require('webpack');
var sass = require('gulp-sass');
var pixrem = require('gulp-pixrem');
var autoprefixer = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var iconfont = require('gulp-iconfont');
var consolidate = require('gulp-consolidate');

var webpackConfig = {
    entry: {
        easycooking: './js/easycooking.js'
    },
    output: {
        filename: '[name].js',
        path: __dirname + '/../web/assets/js',
        publicPath: 'assets/js/'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /(node_modules)/,
                query: {
                    presets: ['react']
                }
            }
        ]
    }
};

/**
 * Default tasks
 */
gulp.task('default', ['dev']);

gulp.task('dev', ['webpack:dev', 'styles:dev']);

gulp.task('build', ['webpack:build', 'styles:build']);

/**
 * Watch
 */
gulp.task('watch', function () {
    gulp.watch('./sass/**/*.scss', ['styles:dev']);
    gulp.watch('./js/**/*.js', ['webpack:dev']);
    gulp.watch('./icons/*.svg', ['icons']);
});

/**
 * Scripts
 */
gulp.task('webpack:dev', function (done) {
    var localConfig = Object.create(webpackConfig);

    localConfig.devtool = 'source-map';
    localConfig.debug = false;

    webpack(localConfig).run(function (err, stats) {
        if (err) throw new gutil.PluginError('webpack:dev', err);

        gutil.log('[webpack:dev]', stats.toString({
            colors: true
        }));

        done();
    });
});

gulp.task('webpack:build', function (done) {
    var localConfig = Object.create(webpackConfig);

    localConfig.output.filename = '[name].min.js';
    localConfig.plugins = [
        new webpack.optimize.UglifyJsPlugin()
    ];

    webpack(localConfig).run(function (err, stats) {
        if (err) throw new gutil.PluginError('webpack:build', err);

        gutil.log('[webpack:build]', stats.toString({
            colors: true
        }));

        done();
    });
});

/**
 * Styles
 */
gulp.task('styles:dev', ['icons'], function () {
    return gulp.src('sass/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            errLogToConsole: true,
            includePaths: ['./node_modules']
        }))
        .pipe(pixrem('1rem'))
        .pipe(autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9', 'and_chr >= 2.3']
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('../web/assets/css'));
});

gulp.task('styles:build', ['styles:dev'], function () {
    return gulp.src('../web/assets/css/*.css')
        .pipe(minifyCSS({
            advanced: false,
            aggressiveMerging: false,
            keepSpecialComments: 0
        }))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('../web/assets/css'));
});

/**
 * Icons
 */
gulp.task('icons', function () {
    return gulp.src('./icons/*.svg')
        .pipe(iconfont({
            fontName: 'easycooking-icons',
            appendUnicode: false,
            formats: ['ttf', 'eot', 'svg', 'woff'],
            timestamp: 1,
            normalize: true
        }))
        .on('glyphs', function (glyphs, options) {
            gulp.src('./sass/templates/_icons.scss')
                .pipe(consolidate('lodash', {
                    glyphs: glyphs,
                    fontName: 'easycooking-icons',
                    fontPath: '../fonts/',
                    className: 'icon'
                }))
                .pipe(gulp.dest('./sass'));
        })
        .pipe(gulp.dest('./../web/assets/fonts/'));
});