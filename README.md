# Easy cooking

Sample Application for PHP/Symfony Workshop. Visit: http://devdu.de

## Installation

```
git clone https://gitlab.com/devdude/easy-cooking.git

cd easy-cooking

composer install

bin/console doctrine:database:create
bin/console doctrine:schema:create
bin/console doctrine:fixtures:load
```